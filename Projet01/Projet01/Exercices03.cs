﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Projet01
{
    class Exercices03
    {
        public static void Run()
        {
            //E01();
            //E02();
            //E03();
            //E04();
            //E05();
            //E06();
            //E07();
            //E08();
            //E08A();
            //E09();
            //E10();
            //E11();
            //E12();


        }
        public static void E01()
        {
            int input;
            string action = "";
            Console.WriteLine("Entrez votre choix [1,2 ou 3] : ");
            do
            {
                input = int.Parse(Console.ReadLine());
            } while (input < 1 || input > 3);

            switch (input)
            {
                case 1: action = "Ajouter"; break;
                case 2: action = "Modifier"; break;
                case 3: action = "Suprimer"; break;
            }
            Console.WriteLine($"{action} une note !");
        }
        public static void E02()
        {
            int input;
            Console.WriteLine("Entrez un nombre entre 10 et 20 inclusivement : ");

            do
            {
                input = int.Parse(Console.ReadLine());
                if (input < 10) Console.WriteLine("Plus grand !!");
                if (input > 20) Console.WriteLine("Plus petit !!");
            } while (input < 10 || input > 20);

            Console.Write("Bravo !!");


        }
        public static void E03()
        {
            int x1, x2, result;
            Console.WriteLine("Entrez deux entier :");
            x1 = int.Parse(Console.ReadLine());
            x2 = int.Parse(Console.ReadLine());
            do
            {
                result = x1 % x2;
                x1 = x2;
                x2 = result;
            } while (result != 0);

            Console.WriteLine($"Le resultat est {x1}");

            // 3.3.1) result = x1 % x2;
            // 3.3.2) 6
            // 3.3.3) 1
            // 3.3.4) Calcule du PGCD  (Plus grand commun diviseur)
        }


        public static void E04()
        {

            int input, x, result = 1;
            Console.WriteLine("Entrez un nombre entier:");
            input = int.Parse(Console.ReadLine());
            x = input;
            while (x != 1)
            {
                result *= result * x;
                x -= x - 1;
            }
            Console.WriteLine($"La factorielle de {input} est {result}:");
        }
        public static void E05()
        {
            int tries = 1;
            int x = new Random().Next(0, 9);
            Console.WriteLine("Quel est le nombre tiré au hasard [0 et 9]");
            int input = int.Parse(Console.ReadLine());
            while (input != x)
            {
                input = int.Parse(Console.ReadLine());
                tries++;
            }
            Console.WriteLine($"Bravo, vous avez réussis en {tries} coup(s)");

        }
        public static void E06()
        {
            int tries = 1;
            int x = new Random().Next(0, 50);

            Console.WriteLine("Quel est le nombre tiré au hasard [0 et 50]");
            int input = int.Parse(Console.ReadLine());

            while (input != x)
            {
                Console.Write("Vous mise trop");
                if (input < x)
                {
                    Console.WriteLine("Bas!");
                }
                else
                {
                    Console.WriteLine("haut!");
                }
                input = int.Parse(Console.ReadLine());
                tries++;
            }


            Console.WriteLine($"Bravo, vous avez réussis en juste {tries} coup(s)!");
        }
        public static void E07()
        {

            Console.WriteLine("Entrez un nombre pair:");
            int x;
            do
            {
                x = int.Parse(Console.ReadLine());
            } while (x % 2 == 1);


            Console.Write("Résultat = [");
            for (int i = 0; i <= 20; i += i + 2)
            {
                Console.Write($"{ x + i }");
                if (i < 19)
                {
                    Console.Write(",");
                }
            }
            Console.WriteLine("]");
        }
        public static void E08()
        {
            int x1, x2;
            Console.WriteLine("Entrer multiplicande :");
            x1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Entrer le nombre de multiplicateurs :");
            x2 = int.Parse(Console.ReadLine());
            Console.WriteLine("");

            for (int i = 1; i <= x2; i++)
            {
                int multiplier = x1 * i;
                Console.WriteLine("{0} x {1} = {2}", x1, i, multiplier);
            }

        }
        public static void E08A()
        {
            int multiplicand, multiplier;
            Console.Write("Entrez le multiplicande :");
            multiplicand = int.Parse(Console.ReadLine());
            Console.Write("Entrez les nombre de multiplicateurs : ");
            multiplier = int.Parse(Console.ReadLine());

            for (int i = 1; i <= multiplier; i++)
            {
                Console.WriteLine($"{multiplicand} x {i} = {multiplicand * i}");
            }
        }
        public static void E09()
        {

            int input, max = 0;
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Entrez un nombre: ");
                input = int.Parse(Console.ReadLine());

                if (input > max)
                {
                    max = input;
                }
            }
            Console.WriteLine($"Le plus grand nombre est: {max}");
        }
        public static void E10()
        {
            int n, a = 0, b = 1, c, i;
            Console.Write("Enter le nombre d'elément de la suite : ");
            n = Int32.Parse(Console.ReadLine());
            for (i = 0; i < n; i++)
            {
                if (i <= 1)
                    c = i;
                else
                {
                    c = a + b;
                    a = b;
                    b = c;
                }
                Console.Write(c + " ");
            }
        }
    }
}
     
                

        


    

       

        

        
    

