﻿using System;
namespace Projet01
{
    public class Exercices01
    {
        public static void Run()
        {
            //E01();
            //E02();
            //E03();
            //E04();
            //E05();
            //E05_2();
            //E06();
            //E07();
            //E08();
            //E09();
            //E10();
            //E11();
            //E12();

        }

        public static void E01()
        {
            int a;
            int b;
            a = 1;
            b = a + 3;
            a = 3;

            String password = "pwk9090!";

            //Console.WriteLine("1.1) a = " + a + " b = " + b);
            Console.WriteLine($"1.1) a = { a }  b = { b } ");
            Console.WriteLine($"Bonjour { password } !");

        }

        public static void E02()
        {
            int a, b, c;
            a = 5;
            b = 3;
            c = a + b;
            a = 2;
            c = b - a;

            Console.WriteLine($"1.2) a = { a }  b = { b } c = { c }");
        }

        public static void E03()
        {
            int a, b;
            a = 5;
            b = a + 4;
            a = a + 1;
            b = a - 4;

            Console.WriteLine($"1.3) a = { a }  b = { b }");
        }

        public static void E04()
        {
            int a, b, c;
            a = 3;
            b = 10;
            c = a + b;
            b = a + b;
            a = c;

            Console.WriteLine($"1.4) a = { a }  b = { b } c = { c }");
        }

        public static void E05()
        {
            int a, b;
            a = 5;
            b = 2;
            a = b;
            b = a;

            Console.WriteLine($"1.5) a = { a }  b = { b }");
        }

        public static void E05_2()
        {
            int a, b;
            a = 5;
            b = 2;
            b = a;
            a = b;

            Console.WriteLine($"1.5.2) a = { a }  b = { b }");
        }

        public static void E06()
        {
            int x, y, temp;
            x = 5;
            y = 8;

            Console.WriteLine($"1.6)\nAvant permutation x = { x } et y = { y }");

            temp = x;
            x = y;
            y = temp;

            Console.WriteLine($"Après permutation x = { x } et y = { y }");

            // {} = accolades / braces
            // [] = crochets / brackets

        }

        public static void E07()
        {
            int a, b, c, temp;
            a = 5;
            b = 10;
            c = 2;

            temp = b;
            b = a;
            a = c;
            c = temp;

            //Il ne suffit que de faire 2 permutations avec une seule variable temporaire
            //Faire la traces des événements est très utile, même pour les gens expérimentés

            Console.WriteLine($"1.7) a = { a }  b = { b } c = { c }");
        }

        public static void E08()
        {
            String a, b, c;
            a = "123";
            b = "456";
            c = a + b;

            //Le signe + dans le contexte d'une chaîne de caractère, concatène les chaînes au lieu de les additionner

            Console.WriteLine($"1.8) c = { c }");
        }

        public static void E09()
        {
            int x, y, z;
            x = 10;
            y = 3;
            z = x / y;

            //Le résultat d'une opération arithmetique de deux int est un int

            Console.WriteLine($"1.9) z = { z }");
        }

        public static void E10()
        {
            int x;
            double y, z;
            x = 10;
            y = 3;
            z = x / y;

            //Le résultat d'une opération arithmetique avec un double donne un double

            Console.WriteLine($"1.10) z = { z }");
        }

        public static void E11()
        {
            double a;
            a = (10 / 3f + 3) / 5d;

            double b;
            b = (10 / 3 + 3) / 5;

            decimal c;
            c = 10.99m + 11.11m;

            float d = (int)(10f / 3f); //Erreur de compilation

            //Le résultat d'une opération arithmetique avec un double donne un double

            Console.WriteLine($"1.11) a = { a } b = { b } c = { c } d = { d }");
        }

        public static void E12()
        {
            String firstname, lastname;
            int age;

            Console.WriteLine("Entrez votre prénom");
            firstname = Console.ReadLine();
            Console.WriteLine("Entrez votre nom");
            lastname = Console.ReadLine();
            Console.WriteLine("Entrez votre année de naissance");
            age = 2020 - int.Parse(Console.ReadLine());

            Console.WriteLine($"Bonjour { firstname } { lastname } tu as { age } ans !");
        }

        
    }
}

        
  
